# KDE Evolution

Project to generate KDE lineage tree from source history and additional investigative research on maintainer knowledge.

## First Notes & Ideas

* rsync the generated archive content lists (*.tar-tv files) from download.kde.org into src-meta
* fetch further project meta data (e.g. group, category, etc) from gitlab into db/{category,maintainer,historical-deps} as a simple ASCII/TSV/CSV tables. NoSQL is probably good to go.
* gather relevant meta data from historical kde-apps.org kde-files.org (crawler)
* run some scripts on the project file history and generate some graphviz.
* try some forensic methods (https://ssdeep-project.github.io/ssdeep/usage.html)
* comparative timetable of versioning activity
    * first occurrence of project archive
    * date of latest release
* ask maintainers to fill the gaps and connect the dots (pose specific questions)
    * was a project renamed?
    * where projects merged or disassembled?
* try to build an evidence/source based lineage tree
* find simple read and writable notations for the db/[table-names] to enable informants/maintainers to easily add relevant metadata
    * db/maintainer: [maintainer]  [project-name] [from] [to] [detail]
    * db/lineage: [project-A]   [replaced/influenced/precedes]   [project-B]
    * db/deps: [project-A]   [is-part-of/depends-on] [project-B]
* keep trees and graphs coarse-meshed, research technical details if necessary (code-level;include-graph)

